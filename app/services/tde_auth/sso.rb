# frozen_string_literal: true

module TdeAuth
  module Sso
    def self.auth(login, password)
      hashed_password = Digest::MD5.hexdigest(password)
      url = SiteSetting.tde_sso_url
      body = { connexion: { login: login, password: hashed_password } }
      response = Faraday.post(url, body)
      JSON.parse(response.body) if response.success?
    end
  end
end
