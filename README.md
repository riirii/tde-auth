# TDEAuth

TdeAuth is a plugin for authentication using TDE SSO

## Installation

Follow [Install a Plugin](https://meta.discourse.org/t/install-a-plugin/19157)
how-to from the official Discourse Meta, using `git clone https://github.com//tde-auth.git`
as the plugin command.
