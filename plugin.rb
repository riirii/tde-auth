# frozen_string_literal: true

# name: TdeAuth
# about:
# version: 0.1
# authors:
# url: https://github.com/

enabled_site_setting :tde_auth_enabled
enabled_site_setting :tde_sso_url

PLUGIN_NAME ||= 'TdeAuth'

load File.expand_path('lib/tde_auth/engine.rb', __dir__)

after_initialize do
  # https://github.com/discourse/discourse/blob/master/lib/plugin/instance.rb

  ::SessionController.class_eval do
    def create
      unless params[:second_factor_token].blank?
        RateLimiter.new(nil, "second-factor-min-#{request.remote_ip}", 3, 1.minute).performed!
      end

      params.require(:login)
      params.require(:password)

      auth_service = ::TdeAuth::Auth.new(params[:login], params[:password])

      if (user = auth_service.user)
        # If the site requires user approval and the user is not approved yet
        if login_not_approved_for?(user)
          render json: login_not_approved
          return
        end

        # User signed on with username and password, so let's prevent the invite link
        # from being used to log in (if one exists).
        Invite.invalidate_for_email(user.email)
      else
        invalid_credentials
        return
      end

      if (payload = login_error_check(user))
        render json: payload
      else
        if params[:second_factor_token].blank?
          security_key_valid = ::Webauthn::SecurityKeyAuthenticationService.new(
            user,
            params[:security_key_credential],
            challenge: secure_session["staged-webauthn-challenge-#{user.id}"],
            rp_id: secure_session["staged-webauthn-rp-id-#{user.id}"],
            origin: Discourse.base_url
          ).authenticate_security_key
          return invalid_security_key(user) if user.security_keys_enabled? && !security_key_valid
        end

        if user.totp_enabled? &&
          !user.authenticate_second_factor(params[:second_factor_token], params[:second_factor_method].to_i) &&
          !params[:security_key_credential].present?
          return render json: failed_json.merge(
            error: I18n.t("login.invalid_second_factor_code"),
            reason: "invalid_second_factor",
            backup_enabled: user.backup_codes_enabled?
          )
        end

        (user.active && user.email_confirmed?) ? login(user) : not_activated(user)
      end
    rescue ::Webauthn::SecurityKeyError => err
      invalid_security_key(user, err.message)
    end
  end
end
